package ru.vpavlova.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.vpavlova.tm.event.ConsoleEvent;
import ru.vpavlova.tm.listener.AbstractProjectListener;
import ru.vpavlova.tm.endpoint.Session;
import ru.vpavlova.tm.exception.entity.ObjectNotFoundException;
import ru.vpavlova.tm.util.TerminalUtil;

@Component
public class ProjectByIdFinishListener extends AbstractProjectListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-finish-status-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Finish project status by id.";
    }

    @Override
    @EventListener(condition = "@projectByIdFinishListener.name() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER ID:");
        if (sessionService == null) throw new ObjectNotFoundException();
        @Nullable final Session session = sessionService.getSession();
        @NotNull final String id = TerminalUtil.nextLine();
        projectEndpoint.finishProjectById(session, id);
    }
    
}
