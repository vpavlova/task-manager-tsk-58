package ru.vpavlova.tm.listener;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.vpavlova.tm.endpoint.AdminEndpoint;
import ru.vpavlova.tm.endpoint.Task;
import ru.vpavlova.tm.endpoint.TaskEndpoint;
import ru.vpavlova.tm.exception.entity.TaskNotFoundException;

import java.util.Optional;

@Component
public abstract class AbstractTaskListener extends AbstractListener {

    @Nullable
    @Autowired
    protected TaskEndpoint taskEndpoint;

    @Nullable
    @Autowired
    protected AdminEndpoint adminEndpoint;

    protected void showTask(@Nullable final Task task) {
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + task.getStatus().value());
        System.out.println("Start Date: " + task.getDateStart());
        System.out.println("Finish Date: " + task.getDateFinish());
        System.out.println("Created: " + task.getCreated());
    }

}
