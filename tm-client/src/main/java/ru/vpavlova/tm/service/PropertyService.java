package ru.vpavlova.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import ru.vpavlova.tm.api.service.IPropertyService;

@Service
@AllArgsConstructor
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @NotNull
    private final Environment environment;

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "1";

    @NotNull
    private static final String DEVELOPER_NAME_KEY = "name";

    @NotNull
    private static final String DEVELOPER_NAME_DEFAULT = "";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "application.version";

    @NotNull
    private static final String APPLICATION_VERSION_DEFAULT = "";

    @NotNull
    private static final String DEVELOPER_EMAIL_KEY = "developer.email";

    @NotNull
    private static final String DEVELOPER_EMAIL_DEFAULT = "vpavlova@tsconsulting.com";

    @NotNull
    private static final String DEVELOPER_COMPANY_KEY = "developer.company";

    @NotNull
    private static final String DEVELOPER_COMPANY_DEFAULT = "TSC";

    @NotNull
    public static final String SIGN_ITERATION = "sign.iteration";

    @NotNull
    public static final String SIGN_ITERATION_DEFAULT = "1";

    @NotNull
    public static final String SIGN_SECRET = "sign.secret";

    @NotNull
    public static final String SIGN_SECRET_DEFAULT = "";

        @NotNull
    @Override
    public String getPasswordSecret() {
            return environment.getProperty(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        @NotNull final String value = environment.getProperty(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
        return Integer.valueOf(value);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        @Nullable final String version;
        if (Manifests.exists(APPLICATION_VERSION_KEY)) version = Manifests.read(APPLICATION_VERSION_KEY);
        else version = environment.getProperty(APPLICATION_VERSION_KEY, APPLICATION_VERSION_DEFAULT);
        return version;
    }

    @NotNull
    @Override
    public String getDeveloperName() {
        @Nullable String name;
        if (Manifests.exists(DEVELOPER_NAME_KEY)) name = Manifests.read(DEVELOPER_NAME_KEY);
        else name = environment.getProperty(DEVELOPER_NAME_KEY, DEVELOPER_NAME_DEFAULT);
        return name;
    }

    @NotNull
    @Override
    public String getDeveloperEmail() {
        @Nullable String email;
        if (Manifests.exists(DEVELOPER_EMAIL_KEY)) email = Manifests.read(DEVELOPER_EMAIL_KEY);
        else email = environment.getProperty(DEVELOPER_EMAIL_KEY, DEVELOPER_EMAIL_DEFAULT);
        return email;
    }

    @NotNull
    @Override
    public String getDeveloperCompany() {
        @Nullable String company;
        if (Manifests.exists(DEVELOPER_COMPANY_KEY)) company = Manifests.read(DEVELOPER_COMPANY_KEY);
        else company = environment.getProperty(DEVELOPER_COMPANY_KEY, DEVELOPER_COMPANY_DEFAULT);
        return company;
    }

    @NotNull
    @Override
    public Integer getSignIteration() {
        @NotNull final String value = environment.getProperty(SIGN_ITERATION, SIGN_ITERATION_DEFAULT);
        return Integer.valueOf(value);
    }

    @NotNull
    @Override
    public String getSignSecret() {
        return environment.getProperty(SIGN_SECRET, SIGN_SECRET_DEFAULT);
    }

}