package ru.vpavlova.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.vpavlova.tm.api.service.model.ISessionGraphService;
import ru.vpavlova.tm.configuration.ServerConfiguration;
import ru.vpavlova.tm.entity.SessionGraph;
import ru.vpavlova.tm.marker.DBCategory;
import ru.vpavlova.tm.service.TestUtil;

import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.List;

public class SessionGraphServiceTest {

    @NotNull AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    private final ISessionGraphService sessionService = context.getBean(ISessionGraphService.class);

    {
        TestUtil.initUser();
    }

    @Before
    public void before() {
        context.getBean(EntityManagerFactory.class).createEntityManager();
    }

    @After
    public void after() {
        context.getBean(EntityManagerFactory.class).close();
    }

    @Test
    @Category(DBCategory.class)
    public void addAllSessionTest() {
        final List<SessionGraph> sessions = new ArrayList<>();
        final SessionGraph session1 = new SessionGraph();
        final SessionGraph session2 = new SessionGraph();
        sessions.add(session1);
        sessions.add(session2);
        sessionService.addAll(sessions);
        Assert.assertTrue(sessionService.findById(session1.getId()).isPresent());
        Assert.assertTrue(sessionService.findById(session2.getId()).isPresent());
    }

    @Test
    @Category(DBCategory.class)
    public void addSessionTest() {
        final SessionGraph session = new SessionGraph();
        sessionService.add(session);
        Assert.assertNotNull(sessionService.findById(session.getId()));
    }

    @Test
    @Category(DBCategory.class)
    public void clearSessionTest() {
        sessionService.clear();
        Assert.assertTrue(sessionService.findAll().isEmpty());
    }

    @Test
    @Category(DBCategory.class)
    public void findAllSession() {
        final List<SessionGraph> sessions = new ArrayList<>();
        final SessionGraph session1 = new SessionGraph();
        final SessionGraph session2 = new SessionGraph();
        sessions.add(session1);
        sessions.add(session2);
        sessionService.addAll(sessions);
        Assert.assertEquals(2, sessionService.findAll().size());
    }

    @Test
    @Category(DBCategory.class)
    public void findSessionOneByIdTest() {
        final SessionGraph session1 = new SessionGraph();
        final String sessionId = session1.getId();
        sessionService.add(session1);
        Assert.assertNotNull(sessionService.findById(sessionId));
    }

    @Test
    @Category(DBCategory.class)
    public void removeSessionOneByIdTest() {
        final SessionGraph session = new SessionGraph();
        sessionService.add(session);
        final String sessionId = session.getId();
        sessionService.removeById(sessionId);
        Assert.assertFalse(sessionService.findById(sessionId).isPresent());
    }

}
