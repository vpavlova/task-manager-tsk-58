package ru.vpavlova.tm.event;

import lombok.*;
import org.jetbrains.annotations.NotNull;

@Getter
@Setter
@AllArgsConstructor
public final class ConsoleEvent {

    @NotNull
    private final String name;

}

