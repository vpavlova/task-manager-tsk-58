package ru.vpavlova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.vpavlova.tm.api.service.dto.IUserService;
import ru.vpavlova.tm.configuration.ServerConfiguration;
import ru.vpavlova.tm.enumerated.Role;

public class TestUtil {

    @NotNull
    static AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    static IUserService userService = context.getBean(IUserService.class);

    public static void initUser() {
        if (!userService.findByLogin("test").isPresent()) {
            userService.create("test", "test", "test@test.ru");
        }
        if (!userService.findByLogin("test2").isPresent()) {
            userService.create("test2", "test", "test@test.ru");
        }
        if (!userService.findByLogin("admin").isPresent()) {
            userService.create("admin", "admin", Role.ADMIN);
        }
    }

}
