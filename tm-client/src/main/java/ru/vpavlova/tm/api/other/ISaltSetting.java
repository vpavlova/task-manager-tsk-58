package ru.vpavlova.tm.api.other;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;

@Service
public interface ISaltSetting {

    @NotNull
    String getPasswordSecret();

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getDeveloperName();

    @NotNull
    String getDeveloperEmail();

    @NotNull
    String getDeveloperCompany();

    @NotNull
    Integer getSignIteration();

    @NotNull
    String getSignSecret();

}
